<?php get_header(); ?>
<section class="error-404 not-found container-fluid clearfix"><!--bannar-section-starts-->
    <div class="container clearfix"><!--bannar-container-starts-->
        <main id="main" class="site-main" role="main">
                <div class="page-content">
                    <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyseventeen' ); ?></h1>
                    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyseventeen' ); ?></p>
                    <?php get_search_form(); ?>
                </div><!-- .page-content -->
        </main><!-- #main -->
    </div><!-- #primary -->
</section>
<?php get_footer(); ?>