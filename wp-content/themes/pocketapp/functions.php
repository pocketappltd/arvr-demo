<?php 
require_once(TEMPLATEPATH . '/includes/functions/sidebars.php'); 

// function my_function_admin_bar(){ return false; }
// add_filter( 'show_admin_bar' , 'my_function_admin_bar'); 
//Add support for WordPress 3.0's custom menus
add_action( 'init', 'register_my_menu' );
add_theme_support( 'post-thumbnails' ); 
 
//Register area for custom menu
function register_my_menu() {
    register_nav_menu( 'primary-menu', __( 'Primary Menu' ) );
    register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );
}
//redirect contact form7
add_filter('wpcf7_form_action_url', 'wpcf7_custom_form_action_url'); 
function wpcf7_custom_form_action_url() { return 'www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8'; }

function my_function_admin_bar(){ return false; }
add_filter( 'show_admin_bar' , 'my_function_admin_bar');
// remove_filter( 'the_content', 'wpautop' );
// remove_filter( 'the_excerpt', 'wpautop' );
//Add files for menu
function default_enqueue_style() {
	//wp_enqueue_style( 'menu', get_stylesheet_directory_uri().'/css/meanmenu.css', false ); 
}
function default_enqueue_script() {
	//wp_enqueue_script( 'menu-script', get_stylesheet_directory_uri().'/js/jquery.meanmenu.js', false );
}
add_action( 'wp_enqueue_scripts', 'default_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'default_enqueue_script' );

//Enable multisite feature (WordPress 3.0)
define('WP_ALLOW_MULTISITE', true);

add_action( 'init', 'add_custom_taxonomy_news', 0 );
function add_custom_taxonomy_news() {
register_taxonomy('news-category', 'News', array(
  'hierarchical' => true,
  'labels' => array(
    'name' => _x( 'News categories', 'taxonomy general name' ),
    'singular_name' => _x( 'News category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search News categories' ),
    'all_items' => __( 'All News category' ),
    'parent_item' => __( 'Parent News category' ),
    'parent_item_colon' => __( 'Parent News category:' ),
    'edit_item' => __( 'Edit News category' ),
    'update_item' => __( 'Update News category' ),
    'add_new_item' => __( 'Add New News category' ),
    'new_item_name' => __( 'New News category Name' ),
    'menu_name' => __( 'News categories' ),
  ),
  'rewrite' => array(
    'slug' => 'news-categories',
    'with_front' => false,
    'hierarchical' => true
  ),
));
}
add_action( 'init', 'create_post_type_news' );
  function create_post_type_news() {
    register_post_type( 'News',
    array(
        'labels' => array(
            'name' => __( 'News' ),
            'singular_name' => __( 'News'),
            'add_new' => __( 'Add New' ),
            'add_new_item' => __( 'Add a New News' ),
            'edit' => __( 'Edit' ),
            'edit_item' => __( 'Edit News' ),
            'new_item' => __( 'New News' ),
            'view' => __( 'View' ),
            'view_item' => __( 'View News' ),
            'search_items' => __( 'Search News' ),
            'not_found' => __( 'No News found' ),
            'not_found_in_trash' => __( 'No News found in Trash' ),
            ),
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),
        'has_archive' => true,
        'menu_position' => 10,
        'public' => true,
        'rewrite' => array( 'slug' => 'all-news' ),
        'taxonomies' => array('news-category')
    )
);
}

add_action( 'init', 'add_custom_taxonomy_work', 0 );
function add_custom_taxonomy_work() {
register_taxonomy('work-category', 'Work', array(
  'hierarchical' => true,
  'labels' => array(
    'name' => _x( 'Work categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Work category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Work categories' ),
    'all_items' => __( 'All Work category' ),
    'parent_item' => __( 'Parent Work category' ),
    'parent_item_colon' => __( 'Parent Work category:' ),
    'edit_item' => __( 'Edit Work category' ),
    'update_item' => __( 'Update Work category' ),
    'add_new_item' => __( 'Add New Work category' ),
    'new_item_name' => __( 'New Work category Name' ),
    'menu_name' => __( 'Work categories' ),
  ),
  'rewrite' => array(
    'slug' => 'case-studies',
    'with_front' => false,
    'hierarchical' => true,
  ),
));
}
add_action( 'init', 'create_post_type_work' );
  function create_post_type_work() {
    register_post_type( 'Work',
    array(
        'labels' => array(
            'name' => __( 'Work' ),
            'singular_name' => __( 'Work'),
            'add_new' => __( 'Add New' ),
            'add_new_item' => __( 'Add a New Work' ),
            'edit' => __( 'Edit' ),
            'edit_item' => __( 'Edit Work' ),
            'new_item' => __( 'New Work' ),
            'view' => __( 'View' ),
            'view_item' => __( 'View Work' ),
            'search_items' => __( 'Search Work' ),
            'not_found' => __( 'No Work found' ),
            'not_found_in_trash' => __( 'No Work found in Trash' ),
            ),
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),
        'has_archive' => true,
        'menu_position' => 10,
        'public' => true,
        'rewrite' => array( 'slug' => 'case-study' ),
        'taxonomies' => array('work-category')
    )
);
}

add_action( 'init', 'register_cpt_team' );


function register_cpt_team() {



    $labels = array( 

        'name' => _x( 'Team', 'team' ),

        'singular_name' => _x( 'Team', 'team' ),

        'add_new' => _x( 'Add New', 'team' ),

        'add_new_item' => _x( 'Add New Team', 'team' ),

        'edit_item' => _x( 'Edit Team', 'team' ),

        'new_item' => _x( 'New Team', 'team' ),

        'view_item' => _x( 'View Team', 'team' ),

        'search_items' => _x( 'Search Team', 'team' ),

        'not_found' => _x( 'No Team found', 'team' ),

        'not_found_in_trash' => _x( 'No Team found in Trash', 'team' ),

        'parent_item_colon' => _x( 'Parent Team:', 'team' ),

        'menu_name' => _x( 'Team', 'team' ),

    );



    $args = array( 

        'labels' => $labels,

        'hierarchical' => true,

        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),

        'public' => true,

        'show_ui' => true,

        'show_in_menu' => true,

        'show_in_nav_menus' => true,

        'publicly_queryable' => true,

        'exclude_from_search' => false,

        'has_archive' => true,

        'query_var' => true,

        'can_export' => true,

        'rewrite' => true,

        'capability_type' => 'post'

    );



    register_post_type( 'team', $args );

}

add_action( 'init', 'register_cpt_process' );


function register_cpt_process() {



    $labels = array( 

        'name' => _x( 'Process', 'process' ),

        'singular_name' => _x( 'Process', 'process' ),

        'add_new' => _x( 'Add New', 'process' ),

        'add_new_item' => _x( 'Add New Process', 'process' ),

        'edit_item' => _x( 'Edit Process', 'process' ),

        'new_item' => _x( 'New Process', 'process' ),

        'view_item' => _x( 'View Process', 'process' ),

        'search_items' => _x( 'Search Process', 'process' ),

        'not_found' => _x( 'No Process found', 'process' ),

        'not_found_in_trash' => _x( 'No Process found in Trash', 'process' ),

        'parent_item_colon' => _x( 'Parent Process:', 'process' ),

        'menu_name' => _x( 'Our Process', 'process' ),

    );



    $args = array( 

        'labels' => $labels,

        'hierarchical' => true,

        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),

        'public' => true,

        'show_ui' => true,

        'show_in_menu' => true,

        'show_in_nav_menus' => true,

        'publicly_queryable' => true,

        'exclude_from_search' => false,

        'has_archive' => true,

        'query_var' => true,

        'can_export' => true,

        'rewrite' => true,

        'capability_type' => 'post'

    );



    register_post_type( 'process', $args );

}

add_action( 'init', 'register_cpt_testimonial' );


function register_cpt_testimonial() {



    $labels = array( 

        'name' => _x( 'Testimonial', 'testimonial' ),

        'singular_name' => _x( 'Testimonial', 'testimonial' ),

        'add_new' => _x( 'Add New', 'testimonial' ),

        'add_new_item' => _x( 'Add New Testimonial', 'testimonial' ),

        'edit_item' => _x( 'Edit Testimonial', 'testimonial' ),

        'new_item' => _x( 'New Testimonial', 'testimonial' ),

        'view_item' => _x( 'View Testimonial', 'testimonial' ),

        'search_items' => _x( 'Search Testimonial', 'testimonial' ),

        'not_found' => _x( 'No Testimonial found', 'testimonial' ),

        'not_found_in_trash' => _x( 'No Testimonial found in Trash', 'testimonial' ),

        'parent_item_colon' => _x( 'Parent Testimonial:', 'testimonial' ),

        'menu_name' => _x( 'Testimonials', 'testimonial' ),

    );



    $args = array( 

        'labels' => $labels,

        'hierarchical' => true,

        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),

        'public' => true,

        'show_ui' => true,

        'show_in_menu' => true,

        'show_in_nav_menus' => true,

        'publicly_queryable' => true,

        'exclude_from_search' => false,

        'has_archive' => true,

        'query_var' => true,

        'can_export' => true,

        'rewrite' => true,

        'capability_type' => 'post'

    );



    register_post_type( 'testimonial', $args );

}

add_action( 'widgets_init', 'add_axit_widgets' );

function add_axit_widgets() {

    register_sidebar( array(

    'name' => 'Topbutton Menu',

    'id' => 'topbutton-menu',

    'description' => 'Appears Header',

    'before_widget' => '',

    'after_widget' => '',

    'before_title' => '<h3 class="widget-title">',

    'after_title' => '</h3>',

    ) );

    register_sidebar( array(

    'name' => 'Find Us',

    'id' => 'find-us',

    'description' => 'Appears Contact page',

    'before_widget' => '',

    'after_widget' => '',

    'before_title' => '<h3 class="widget-title">',

    'after_title' => '</h3>',

    ) );

    register_sidebar( array(

    'name' => 'Social Links',

    'id' => 'social-links',

    'description' => 'Appears Contact page',

    'before_widget' => '',

    'after_widget' => '',

    'before_title' => '<h3 class="widget-title">',

    'after_title' => '</h3>',

    ) );

    register_sidebar( array(

    'name' => 'Other Office',

    'id' => 'other-office',

    'description' => 'Appears Contact page',

    'before_widget' => '',

    'after_widget' => '',

    'before_title' => '<h3 class="widget-title">',

    'after_title' => '</h3>',

    ) );

    register_sidebar( array(

    'name' => 'Work Discucss Projects',

    'id' => 'work-discuss-projects',

    'description' => 'Appears Work Page',

    'before_widget' => '',

    'after_widget' => '',

    'before_title' => '<h4 class="widget-title">',

    'after_title' => '</h4>',

    ) );

    register_sidebar( array(

    'name' => 'Get In Touch Form',

    'id' => 'get-in-touch-form',

    'description' => 'Appears Work Page',

    'before_widget' => '',

    'after_widget' => '',

    'before_title' => '<h4 class="widget-title">',

    'after_title' => '</h4>',

    ) );

    /*------------------------------------------------------------------------------------------*/
    function custom_pagination($numpages = '', $pagerange = '', $paged='') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }
    $pagination_args = array(
        'base'            => get_pagenum_link(1) . '%_%',
        'format'          => 'page/%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => False,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => True,
        'prev_text'       => __('Previous page'),
        'next_text'       => __('Next page'),
        'type'            => 'plain',
        'add_args'        => false,
        'add_fragment'    => ''
    );

    $paginate_links = paginate_links($pagination_args);

    if ($paginate_links) {
        echo "<nav class='custom-pagination'>";
        //echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
        echo $paginate_links;
        echo "</nav>";
    }

    }


function fix_taxonomy_pagination ( $query ) {
  // not an admin page and it is the main query
  if (!is_admin() && $query->is_main_query()){

    if(is_tax()){
      // where 24 is number of posts per page on custom taxonomy pages
      $query->set('posts_per_page', 4);

    }
  }
}
add_action( 'pre_get_posts', 'fix_taxonomy_pagination' );


} 
function gmpa_set_default_object_terms( $post_id, $post ) {
    if ( 'publish' === $post->post_status && $post->post_type === 'news' ) {
        $defaults = array(
        'news-category' => array( 'all' )
        //'your_taxonomy_id' => array( 'your_term_slug', 'your_term_slug' )
        );
 
        $taxonomies = get_object_taxonomies( $post->post_type );
        foreach ( (array) $taxonomies as $taxonomy ) {
            $terms = wp_get_post_terms( $post_id, $taxonomy );
            if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
                wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
            }
        }
    }
}
add_action( 'save_post', 'gmpa_set_default_object_terms', 100, 2 );