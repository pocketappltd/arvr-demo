<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>
<section class="search-page container-fluid clearfix"><!--bannar-section-starts-->
    <div class="container clearfix"><!--bannar-container-starts--> 
        <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'default-theme' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
        <?php if(have_posts()) : ?>
        <div class="searchpage-wrap">
        <?php while(have_posts()) : the_post(); ?>
        <article>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
             <div class="entry">
            <?php the_content(); ?>
            <p class="blog-dt"><?php _e('Written by:'); ?> <?php  the_author(); ?> <?php _e('On:'); ?> <?php the_date() ?> </p>
            </div>
 
        </article>
        <?php endwhile; ?>
        </div> 
         
            <div class="navigation">
                <?php posts_nav_link(); ?>
            </div>
         

        <?php else: ?>
                <h3>No post found. Try a Different Search</h3>
        <?php endif; ?>
    </div>
</section>
<?php get_footer(); ?>