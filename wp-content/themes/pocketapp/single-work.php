<?php get_header(); ?>

<section class="work-section-single container-fluid clearfix">
    <div class="container clearfix">
    <a class="blog-dt go-back mobile" href='javascript:history.back(1);'>Back to Our Work</a>
        <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>        
                <article>
                <div class="top-title-wrap">
                   <h1 class="page-title"><?php the_title(); ?></h1>
                   <span class="blog-cat"><?php echo get_the_term_list( $post->ID, 'work-category', '', ' ' ); ?></span>
                </div>
                <div class="challenge-sec clearfix">
                  <div class="app-logo"><img src="<?php echo get_field('app_icon'); ?>"></div>
                  <div class="challenge-content"><?php echo get_field('the_challenge_section'); ?></div>
                </div>
                <div class="our-approach-sec clearfix">
                 <?php echo get_field('our_approach_section'); ?> 
                </div>
                <div class="screens-sec clearfix">
                 <div class="singlework-inner-wrapper owl-carousel owl-theme singlework-carousel">
                   <?php foreach( get_cfc_meta('screens') as $key => $value ) : ?>
                    <div class="screen-list"><a href="<?php the_cfc_field( 'screens','screen-images', false, $key ); ?>" data-fancybox="gallery" class="related-img"><img src="<?php the_cfc_field( 'screens','screen-images', false, $key ); ?>"></a></div>
                    <?php endforeach; ?>
                  </div>
                </div>
                <blockquote class="tag-line clearfix">
                    <p><?php echo get_field('quotation_content'); ?></p>
                    <h5><?php echo get_field('written_by'); ?></h5>
                </blockquote>    
               </article>
            <?php endwhile; ?>
            <section class="related-projects-blogs clearfix">
                <h1>Other related projects</h1>  
                <?php 
                    //Get array of terms
                    $terms = get_the_terms( $post->ID , 'work-category', 'string');
                    //Pluck out the IDs to get an array of IDS
                    $term_ids = wp_list_pluck($terms,'term_id');

                    //Query posts with tax_query. Choose in 'IN' if want to query posts with any of the terms
                    //Chose 'AND' if you want to query for posts with all terms
                      $second_query = new WP_Query( array(
                          'post_type' => 'Work',
                          'tax_query' => array(
                                        array(
                                            'taxonomy' => 'work-category',
                                            'field' => 'id',
                                            'terms' => $term_ids,
                                            'operator'=> 'IN' //Or 'AND' or 'NOT IN'
                                         )),
                          'posts_per_page' => 2,
                          'ignore_sticky_posts' => 1,
                          'orderby' => 'rand',
                          'post__not_in'=>array($post->ID)
                       ) );

                    //Loop through posts and display...
                        if($second_query->have_posts()) {
                         while ($second_query->have_posts() ) : $second_query->the_post(); ?>
                          <figure class="single_related">
                               <div class="img-left"><img src="<?php echo get_field('related_post_logo'); ?>"></div>
                               <figcaption>
                                  <a href="<?php the_permalink(); ?>"><h2><?php the_title();?></h2></a>
                                  <p><?php echo wp_trim_words( get_the_content($post->ID), 20);?></p>
                                  <a href="<?php the_permalink(); ?>" class="more_btn">VIEW CASE STUDY</a>
                               </figcaption>   
                          </figure>
                       <?php endwhile; wp_reset_query();
                       }
                 ?>
            </section>
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>