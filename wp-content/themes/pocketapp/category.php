<?php get_header(); ?>
 
   <section class="the-team-section container-fluid clearfix">
    <div class="container clearfix">
           <div class="sidebar-subcat-links">
              <div class="category-wrap wow slideInLeft dropdown">
            <?php $the_query = new WP_Query( '' ); ?>
       <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
                         <?php endwhile;?>
                                  <h3 class="product-title">Our Products</h3>
              <ul id="TableData"  class="dropdown-hidden dropdown_categories dropdown_top_level_categories sidebar-subcat-menus">
              <?php 
                          $catargs = array(
                                      'type'=> array('work'),
                                      'orderby' => 'name',
                                      'order' => 'ASC',
                                      'parent' => 0,
                                      'taxonomy' => array('categories'),
                                      'pad_counts' => false 
                                      ); 
                          $catcategories = get_categories($catargs);
                          foreach ($catcategories as $valarr) { ?>

                        <li>
                        <a href="<?php echo get_category_link( $valarr->term_id ); ?>">
                                    <?php echo $valarr->name; ?> 
                                    <?php //echo $termchildren->name; ?> 
                                </a>
                
                                <?php
                                      if (is_category()) {
                                        $this_category = get_category($valarr);
                                      }
                                    ?>
                                    <?php
                                      if($this_category->category_parent)
                                        $this_category = wp_list_categories('orderby=id&show_count=0&title_li=&use_desc_for_title=1&child_of='.$valarr->term_id."&echo=0"); else $this_category = wp_list_categories('orderby=id&show_count=0&title_li=&use_desc_for_title=1&child_of='.$valarr->term_id."&echo=0");
                                     if ($this_category) { ?>
                                       <ul>
                                         <?php echo $this_category; ?>
                                       </ul>
                                <?php } ?>
                       </li>

             <?php   } ?> 
            </ul>
           </div> 
        </div>
        <div class="main-right">
        <div class="cat-page-contant-side" >
        <?php
          $currCat = get_category(get_query_var('cat'));
          $cat_name = $currCat->name;
          $cat_id   = get_cat_ID( $cat_name );
        ?>

            <?php
              $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
              $temp = $wp_query;
              $wp_query = null;
              $wp_query = new WP_Query();
              $wp_query->query('showposts=-1&post_type=work&paged='.$paged.'&cat='.$cat_id);
              while ($wp_query->have_posts()) : $wp_query->the_post();
            ?>
            <!-- heml goes here -->

            <a href="<?php the_permalink(); ?>" class="product wow zoomIn">
                <div class="product-image">
                     <?php if ( has_post_thumbnail() ) {
                                     the_post_thumbnail();
                                     } 
                                    else { ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/image_placeholder.jpg" alt="<?php the_title(); ?>" />
                    <?php } ?>
                    <?//php the_post_thumbnail(full); ?>
                    <div class="product-arrow"> 
                        <i class="fa fa-share" aria-hidden="true"></i>
                    </div>  
                </div>
                <div class="product-details">
                    <div class="product-name">
                        <?php the_title(); ?>
                    </div>
                </div>
            </a>
            <!-- heml goes here -->

            <?php endwhile; ?>
            <?php
              global $wp_query;
             
              $big = 999999999; // need an unlikely integer
              echo '<div class="paginate-links-cat">';
                echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'prev_text' => __('<<'),
                'next_text' => __('>>'),
                'current' => max( 1, get_query_var('paged') ),
                'total' => $wp_query->max_num_pages
                ) );
              echo '</div>';
            ?>

        </div>
        </div>
    </div>
  </section>
 
<?php get_sidebar(); ?>   
<?php get_footer(); ?>