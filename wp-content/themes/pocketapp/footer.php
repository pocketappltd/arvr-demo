</main>
<!-- <div class="mobile_menu">
    <div  class="open">
	    <span class="cls"></span>
	    <span><?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'sub-menu', 'theme_location' => 'primary-menu' ) ); ?></span>
	    <span class="cls"></span>
    </div>	
</div> -->
<!--Main Ends-->
<!--Footer Starts-->  
<footer>
	<!-- <div class="container clearfix">
	   <p class="footer-left" role="contentinfo">® Copyright <?php bloginfo('name'); ?> 2017. All rights reserved.</p> 
	   <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav footer-right', 'theme_location' => 'footer-menu' ) ); ?>
	</div> -->
</footer>
<!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.fancybox.min.js"></script> 
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js"></script> -->

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/index.js"></script>

 <?php wp_footer(); ?>
 <!--Footer Ends--> 
<?php
	$qs = "";
	if(isset($_GET['os']) && !empty($_GET['os'])){
		$qs = $_GET['os'];
	}
	
	/* Variable initialization */
	$strQueryString	 	= (isset($_SERVER["REQUEST_URI"]) && ($_SERVER["REQUEST_URI"] != ''))?$_SERVER["REQUEST_URI"]:'';
	$strQueryString		= explode("?",$strQueryString);
	$strQueryString		= $strQueryString[0];
	
	/* echo "<pre>";
	print_r($_SERVER);
	exit; */
 ?>
 <script type="text/javascript">
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                items:1,
                loop:true,
                margin:10
            });
			
			var qs = "<?php  echo $qs; ?>";
			var OS = getMobileOperatingSystem();
			var device = getDevice();
			if(OS == "iOS" || qs == "ios"){
				$("#avail-android").show();
				$("#android").hide();
				$("#ios_row").show();
				if(device == "Mobile"){
					$("#os-text").html('<a role="button" class="btn btn-primary" href="'+$("#android").attr('href')+'">Android</a>');
				}else if(device == "Desktop"){
					$("#os-text").html('<a role="button" class="btn btn-primary" href="<?php echo get_site_url().$strQueryString."?os=android"; ?>">Android</a>');
					$("#iOS").attr('href', $("#iOS").attr('p-list'));
				}
				
			}else if(OS != "iOS" || qs == "android"){
				$("#avail-ios").show();
				$("#iOS").hide();
				$("#android_row").show();
				if(device == "Mobile"){
					$("#os-text").html('<a role="button" class="btn btn-primary" href="'+$("#iOS").attr('p-list')+'">iPhone</a>');
				}else if(device == "Desktop"){
					$("#os-text").html('<a role="button" class="btn btn-primary" onclick="setURL()" href="<?php echo get_site_url().$strQueryString."?os=ios"; ?>">iPhone</a>');
				}
			}
        });
		
    </script>

</body>
</html>