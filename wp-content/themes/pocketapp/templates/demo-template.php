<?php 
/*
Template Name: Demo Template
*/
?>
<?php get_header(); ?>
<header class="header container-fluid" ><!-- Header Starts -->
				<div class="top_menu_bar container clearfix">
					<div class="top_menu_left" role="banner"> 
						<a class="navbar-brand" href="<?php echo get_option('home'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo@2x.png" alt="Pocketapp logo"></a>
					</div>
					<div class="top_menu_right">
						<nav class="navbar" role="navigation">
							  <!-- <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav navbar-nav', 'theme_location' => 'primary-menu' ) ); ?> -->
						</nav>
							<?php
								  /*if(is_active_sidebar('topbutton-menu')){
								  dynamic_sidebar('topbutton-menu');
								  }*/
							 ?>        
					</div>                
				</div>         
</header><!-- Header Ends -->
<!--Header Ends--> 
<!--Main Starts-->
<main role="main">
<section class="the-team-section container-fluid clearfix">
    <div class="container clearfix">
        <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>        
                <article>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    <div class="entry">   
                        <?php the_post_thumbnail(); ?>
                        <?php the_content(); ?>
         
                        <!-- <div class="postmetadata">
                        <p><?//php _e('Written by:'); ?> <?php  the_author(); ?> <?//php _e('On:'); ?> <?//php the_date() ?> </p>
                        <p><?//php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?> <?//php edit_post_link('Edit', ' &#124; ', ''); ?></p>
                        </div> -->
         
                    </div>
         
                    <!-- <div class="comments-template">
                        <?//php comments_template(); ?>
                    </div> -->
               </article>
            <?php endwhile; ?>
         
            <!-- <div class="navigation">  
                <?//php previous_post_link('< %link') ?> <?//php next_post_link(' %link >') ?>
            </div> -->
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>