<?php

get_header(); ?>

	<section class=" container-fluid clearfix">
    <div class="container clearfix">
	        <?php if ( have_posts() ) : ?>
	        <div class="blog-content-wrapper">
					                                <div class="blog-img">
						                                <?php if ( has_post_thumbnail() ) {
						                                 the_post_thumbnail();
						                                 } 
						                                else { ?>
														<img src="<?php bloginfo('template_directory'); ?>/images/image_placeholder.jpg" alt="<?php the_title(); ?>" />
														<?php } ?>
						                             </div>
						                             <h1><?php the_title();?></h1>
						                             <div class="blog-date">
						                                <span class="blog-cat"><?php the_category(' , '); ?></span>
						                                <span class="blog-dt"><?php echo get_the_date( 'jS F Y' );?></span>
					                                 </div>
					                                <p><?php the_content();?></p>
			</div> 
			<?php endif; ?>  
    </div>
  </section>
 

<?php get_sidebar(); ?>
<?php get_footer(); ?>