<?php get_header(); ?>

<section class="news-section-single-page container-fluid clearfix">
    <div class="container clearfix">
     <a class="blog-dt go-back mobile" href='javascript:history.back(1);'>Back to news</a>
        <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>        
                <article>
                <span class="blog-dt desktop"><?php echo get_the_date( 'j F Y' );?></span>
                <h1><?php the_title(); ?></h1>
                <div class="blog-share-link clearfix">
                    <span class="blog-cat"><?php echo get_the_term_list( $post->ID, 'news-category', '', ', ' ); ?></span>
                    <span class="blog-dt mobile"><?php echo get_the_date( 'j F Y' );?></span>
                    <span class="blog-share"><?php echo do_shortcode('[addtoany buttons="facebook,twitter,google_plus"]');?></span>
                </div>
                    <div class="news-blog-content">
                        <?php the_content(); ?>   
                        <?//php the_post_thumbnail(); ?> 
                        <span class="blog-share"><label>Share with</label><?php echo do_shortcode('[addtoany buttons="facebook,twitter,google_plus"]');?></span> 
                    </div>
               </article>
            <?php endwhile; ?>
         
            <section class="related-news-blogs clearfix">  
                <?php 
                    //Get array of terms
                    $terms = get_the_terms( $post->ID , 'news-category', 'string');
                    //Pluck out the IDs to get an array of IDS
                    $term_ids = wp_list_pluck($terms,'term_id');

                    //Query posts with tax_query. Choose in 'IN' if want to query posts with any of the terms
                    //Chose 'AND' if you want to query for posts with all terms
                      $second_query = new WP_Query( array(
                          'post_type' => 'News',
                          'tax_query' => array(
                                        array(
                                            'taxonomy' => 'news-category',
                                            'field' => 'id',
                                            'terms' => $term_ids,
                                            'operator'=> 'IN' //Or 'AND' or 'NOT IN'
                                         )),
                          'posts_per_page' => 2,
                          'ignore_sticky_posts' => 1,
                          'orderby' => 'rand',
                          'post__not_in'=>array($post->ID)
                       ) );

                    //Loop through posts and display...
                        if($second_query->have_posts()) {
                         while ($second_query->have_posts() ) : $second_query->the_post(); ?>
                          <article class="single_related">
                            <a href="<?php the_permalink(); ?>"><h3><?php the_title();?></h3></a>
                                 <div class="blog-date">
                                            <span class="blog-cat"><?php echo get_the_term_list( $post->ID, 'news-category', '', ', ' ); ?></span>
                                            <span class="blog-dt"><?php echo get_the_date( 'j F Y' );?></span>
                                 </div>
                            <p><?php echo wp_trim_words( get_the_content($post->ID), 30);?></p>   
                          </article>
                       <?php endwhile; wp_reset_query();
                       }
                 ?>
            </section>
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>