jQuery(document).ready(function($) {

    $(document).delegate('.open', 'click', function(event) {
        $(this).addClass('oppenned');
        event.stopPropagation();
    });
    $(document).delegate('body', 'click', function(event) {
        $('.open').removeClass('oppenned');
    });
    $(document).delegate('.cls', 'click', function(event) {
        $('.open').removeClass('oppenned');
        event.stopPropagation();
    });

    //
    var pgurl = window.location.href;
    $(".sidebar-subcat-menus li").each(function() {
        if ($(this).find('a').attr("href") == pgurl || $(this).find('a').attr("href") == '')
            $(this).addClass("active");
    });

//

  $(".cross").hide();
  $(".sidebar-subcat-menus").hide();
  $(".hamburger").click(function() {
      $(".sidebar-subcat-menus").slideToggle(250, function() {
          $(".hamburger").hide();
          $(".cross").show();
      });
  });
$(".cross").click(function() {
  $(".sidebar-subcat-menus").slideToggle(250, function() {
      $(".cross").hide();
      $(".hamburger").show();
  });
});

//
$('.work-carousel').owlCarousel({
    loop: true,
    margin: 10,
    dots: true,
    autoplay: true,
    autoplayTimeout:3000,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            dots: true,
            margin: 20
        },
        600: {
            items: 1,
            dots: true,
            margin: 20
        },
        1000: {
            items: 1,
            loop: false,
            dots: true,
            margin: 20
        }
    }
});

//
$('.singlework-carousel').owlCarousel({
    loop: true,
    margin: 10,
    dots: true,
    autoplay: true,
    autoplayTimeout:3000,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            dots: true,
            margin: 20
        },
        767: {
            items: 1,
            dots: true,
            margin: 20
        },
        768: {
            items: 3,
            dots: true,
            margin: 20
        },
        1000: {
            items: 3,
            loop: false,
            dots: true,
            margin: 20
        }
    }
});

$('.testimonial-carousel').owlCarousel({
    loop: true,
    margin: 10,
    dots: true,
    autoplay: true,
    autoplayTimeout:3000,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            dots: true,
            margin: 20
        },
        600: {
            items: 1,
            dots: true,
            margin: 20
        },
        1000: {
            items: 1,
            loop: false,
            dots: true,
            margin: 20
        }
    }
});
//
    if( window.location.href.match(new RegExp('case-studies/.+')) ) {
        $('body').addClass('workpage-active-link');
    } else if (window.location.href.match('case-study/.+')) {
        $('body').addClass('workpage-active-link');
    }else if (window.location.href.match('news-categories/.+')) {
        $('body').addClass('newspage-active-link');
    }else if (window.location.href.match('all-news/.+')) {
        $('body').addClass('newspage-active-link');
    }

});