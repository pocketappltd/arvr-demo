<?php
/**
 * news_category taxonomy archive
 */
get_header(); ?>

  <section class="our-work-section container-fluid clearfix">
  <div class="container clearfix">
          <div class="title-wrapper">
          <h1 class="page-title">Our Work</h1>
          </div>
           <div class="sidebar-subcat-links">
                <div class="burger-wrapper clearfix">
                <span class="blog-cat"><?php single_term_title(); ?></span>
                <span class="category-title desktop">Categories</span>
                <button class="hamburger"></button>
                <button class="cross"></button>
                </div>
                <ul class="sidebar-subcat-menus">
                <?php 
                          $catargs = array(
                                       'type'=> array('Work'),
                                        'orderby' => 'name',
                                        'order' => 'ASC',
                                        'parent' => 0,
                                        'taxonomy' => array('work-category'),
                                        'pad_counts' => false 
                                      ); 
                          $catcategories = get_categories($catargs);
                          foreach ($catcategories as $valarr) { ?>

                        <li>
                              <a href="<?php echo get_category_link( $valarr->term_id ); ?>">
                                      <?php echo $valarr->name; ?> 
                              </a>
                        </li>

                <?php   } ?> 
               </ul>
           </div>
          <div class="work-page clearfix">  
                   <?php 
                   $count = 1;
                   while(have_posts()) : the_post(); ?>        
                        <figure class="clearfix <?php if($count % 2 == 0) { echo 'work-postlist-even'; } ?>">
                           <div class="work_list_left">
                               <?php if ( has_post_thumbnail() ) {
                                                   the_post_thumbnail();
                                                   } 
                                                  else { ?>
                                  <img src="<?php bloginfo('template_directory'); ?>/images/image_placeholder.jpg" alt="<?php the_title(); ?>" />
                               <?php } ?>
                           </div>
                           <figcaption style="background-image:url(<?php echo get_field('watermark_image'); ?>)">
                              <div  class="work-wrap">
                               <div class="work-list-post-title">
                               <h3><?php the_title(); ?></h3>
                               <span class="blog-cat"><?php echo get_the_term_list( $post->ID, 'work-category', '', ' ' ); ?></span>
                               </div>
                               <p><?php echo wp_trim_words( get_the_content($post->ID), 20);?></p>
                               <a href="<?php the_permalink(); ?>" class="more_btn">VIEW CASE STUDY</a>
                              </div>
                           </figcaption>
                        </figure>
                        <?php 
                        if($count == 2) {
                          echo '<div class="discuss-project clearfix">';

                                if(is_active_sidebar('work-discuss-projects')){
                                dynamic_sidebar('work-discuss-projects');
                                }
                          echo '</div>';
                        } ?>
                        <!-- -->
            <?php $count++; endwhile;wp_reset_query(); ?>
             <div id="pagination" class="clearfix">
                <?php //posts_nav_link(); 
                if (function_exists(custom_pagination)) {
                    custom_pagination($loop->max_num_pages,"",$paged);
                }?>
                
            </div>
            <div class="work-testimonial-list" >
              <?php  $args = array( 'post_type' => 'testimonial', 'posts_per_page' => 1, 'orderby' => 'rand');
                              global $post;
                              $loop = new WP_Query( $args );
                           while ( $loop->have_posts() ) : $loop->the_post(); ?>
                 <figure class="clearfix">
                   <div class="work_list_image"><?php the_post_thumbnail(); ?></div>
                   <figcaption>
                     <p><?php echo get_the_content();?></p>
                     <h3><?php the_title();?></h3>
                   </figcaption>
                 </figure>
              <?php endwhile;wp_reset_query(); ?>
            </div>
        </div>
    </div>
  </section>


<?php get_footer(); ?>