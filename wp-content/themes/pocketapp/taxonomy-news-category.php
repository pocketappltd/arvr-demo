<?php
/**
 * news_category taxonomy archive
 */
get_header(); ?>

  <section class="news-section container-fluid clearfix">
    <div class="container clearfix">
      <div class="title-wrapper">
      <h1 class="page-title">News</h1>
      </div>
          <div class="sidebar-subcat-links">
                <div class="burger-wrapper clearfix">
                <span class="blog-cat"><?php single_term_title(); ?></span>
                <span class="category-title desktop">Categories</span>
                <button class="hamburger"></button>
                <button class="cross"></button>
                </div>
                <ul class="sidebar-subcat-menus">
                          <?php 
                            $catargs = array(
                                        'type'=> array('News'),
                                        'orderby' => 'name',
                                        'order' => 'ASC',
                                        'parent' => 0,
                                        'taxonomy' => array('news-category'),
                                        'pad_counts' => false 
                                        ); 
                            $catcategories = get_categories($catargs);
                          foreach ($catcategories as $valarr) { 
                          ?>
                          <li>
                              <a href="<?php echo get_category_link( $valarr->term_id ); ?>">
                                      <?php echo $valarr->name; ?> 
                              </a>
                          </li>

                           <?php } ?> 
              </ul>
          </div>
          <section class="singlenews-post clearfix">
          <?php 
           $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 

               //echo $term->name;
               $args = array(
              'post_type' => 'News',
              "posts_per_page" => 1,
              'tax_query' => array(
                  array(
                       'taxonomy' => 'news-category',
                          'field' => 'term_id',
                          'terms' => $term->term_id,
                  ),
              ),
           );
          //echo $term->term_id;
           $loops = new WP_Query( $args );
          while( $loops->have_posts()) :  $loops->the_post();  ?>
                    <div class="news-top-content-wrapper">
                                                    <div class="blog-img">
                                                      <?php if ( has_post_thumbnail() ) {
                                                       the_post_thumbnail();
                                                       } 
                                                      else { ?>
                                      <img src="<?php bloginfo('template_directory'); ?>/images/image_placeholder.jpg" alt="<?php the_title(); ?>" />
                                      <?php } ?>
                                                   </div>
                                                   <a href="<?php the_permalink(); ?>"><h2><?php the_title();?></h2></a>
                                                   <div class="blog-date">
                                                      <span class="blog-cat"><?php echo get_the_term_list( $post->ID, 'news-category', '', ', ' ); ?></span>
                                                      <span class="blog-dt"><?php echo get_the_date( 'j F Y' );?></span>
                                                     </div>
                                                    <p><?php echo wp_trim_words( get_the_content($post->ID), 45);?></p>
                </div> 
          <?php endwhile; wp_reset_query();?> 
          </section> 
          <div class="news-right clearfix">
                   <?php while(have_posts()) : the_post(); ?>        
                        <figure class="news-content-list clearfix">  
                             <img src="<?php echo get_field('list_thumbnail_img'); ?>">                        
                            <figcaption>
                              <a href="<?php the_permalink(); ?>"><h3><?php the_title();?></h3></a> 
                              <div class="blog-date">
                                  <span class="blog-cat"><?php echo get_the_term_list( $post->ID, 'news-category', '', ', ' ); ?></span>
                                  <span class="blog-dt"><?php echo get_the_date( 'j F Y' );?></span>
                              </div>
                              <p><?php echo wp_trim_words( get_the_content($post->ID), 32);?></p>
                            </figcaption> 
                        </figure>
             <?php endwhile; wp_reset_query();?>
              <div id="pagination" class="clearfix">
                <?php //posts_nav_link(); 
                if (function_exists(custom_pagination)) {
                    custom_pagination($loop->max_num_pages,"",$paged);
                }?>
                
              </div>
          </div>
    </div>
  </section>
<?php get_footer(); ?>