<?php get_header(); ?>
<div id="wrapper">
	<div class="container">
			<?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>
            <header class="header-bg">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 headerLogo">
                            <!-- <img src="../wp-content/themes/pocketapp/images/vccp.png" alt="VCCP" title="VCCP" class="demologo"> -->
							<?php the_post_thumbnail(); ?>
                        </div>
						<div class="col-xs-12 col-sm-6 headerBtn">
						Also Available <span id="os-text">iPhone</span>
						</div>
                    </div>
            </header>
	</div>
                        <?php the_content(); ?>
            <?php endwhile; ?>
        <?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 pocketLogo">
				<span>Powered by</span> <img src="../wp-content/themes/pocketapp/images/logo.png" class="img-responsive" alt="PocketApp" title="PocketApp" width="162">
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>